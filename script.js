let submit = document.getElementById("submit");

submit.addEventListener("click", () => {
    let table = document.getElementById("table");
    if(table !== null)
        document.body.removeChild(table);
});

submit.addEventListener("click", () => {
    let form = document.myForm;
    let rowsCount = parseInt(form.rows.value);
    let columnsCount = parseInt(form.columns.value);

    if(isNaN(rowsCount) || isNaN(columnsCount) || rowsCount < 1 || columnsCount < 1) {
        alert("Incorrect");
    } else {
        let table = document.createElement("table");
        table.id = "table";
    
        for(let i = 1; i < rowsCount + 1; i++){
            let elem = document.createElement("tr");
            for(let j = 1; j < columnsCount + 1; j++){
                let cell = document.createElement("td");
                elem.appendChild(cell);
            }
            table.appendChild(elem);
        }
        document.body.appendChild(table);
    }
});
